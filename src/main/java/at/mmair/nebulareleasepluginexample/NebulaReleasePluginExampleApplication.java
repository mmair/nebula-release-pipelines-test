package at.mmair.nebulareleasepluginexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NebulaReleasePluginExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(NebulaReleasePluginExampleApplication.class, args);
	}
}
